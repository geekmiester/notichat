import 'package:flutter/material.dart';
import 'homepage.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'NotiChat',
      theme: ThemeData(
        primaryColor: Color(0xffFFFC00),
        //accentColor: Color(0xff2E3B84)
      ),
      home: new HomePage(),
      
      ); 
  }
}