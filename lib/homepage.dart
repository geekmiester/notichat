import 'package:flutter/material.dart';
import 'chatscreen.dart';


class HomePage extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: new Text("Noti Chat", style: TextStyle(color: Colors.black87),),
      ),
      body: new ChatScreen()
    ); 
  }
}